app.controller("chatController", function($scope) {
    $scope.messages = [];
    $scope.message = {
        you: '',
        them: ''
    };

    $scope.sendMessage = function (from) {
        if(from === 0 && $scope.message.you) {
            $scope.messages.push({ from: from, text: $scope.message.you });
            $scope.message.you = '';
        } else if (from === 1 && $scope.message.them) {
            $scope.messages.push({ from: from, text: $scope.message.them });
            $scope.message.them = '';
        }
    }
})